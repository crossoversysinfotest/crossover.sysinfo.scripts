#!/bin/bash

source repositories.sh

# Clone all repos from global REPOSITORIES variable
#
clone_repo() {
for repo in $REPOSITORIES 
do
 local repo_folder=$(get_repo_folder ${repo})
 if [ ! -d ${repo_folder} ]
 then 
   echo "Cloning ${repo} into ${repo_folder}" 
   git clone ${repo}
 else
   echo "Skipping clong ${repo}. It already exist"
 fi
done
}

cd ..
clone_repo

cd -


