
DROP DATABASE IF EXISTS `SysInfo`;

CREATE DATABASE `SysInfo`;

CREATE TABLE `SysInfo`.`MachineID` ( 
  `Id` INT NOT NULL AUTO_INCREMENT , 
  `Name` VARCHAR(200) NOT NULL , 
  `Description` VARCHAR(2000) NOT NULL , 
  `Email` VARCHAR(100) NOT NULL , 
   PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB;

CREATE TABLE `SysInfo`.`MachineEvent` (
  `Id` INT NOT NULL,
  `TimeStamp` DATE NOT NULL,
  `FreeMemory` INT NOT NULL,
  `CpuUsage` INT NOT NULL,
  `CpuTemperature` INT NOT NULL,
  `ProcessCount` INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE `SysInfo`.`MachineEvent` ADD CONSTRAINT `fk_id` FOREIGN KEY (`Id`) REFERENCES `SysInfo`.`MachineID`(`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
