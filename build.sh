#!/bin/bash

source repositories.sh

# Build all repos from global REPOSITORIES variable
#
build_repo() {
for repo in $REPOSITORIES 
do
 local repo_folder=$(get_repo_folder ${repo})
 cd "../"${repo_folder}
 echo "BUILDING REPOSITORY: ${repo_folder}"
 
 if [ -x "crossover_build.sh" ]
 then
  ./crossover_build.sh
 else
  echo "NOTHING TO DO IN ${repo_folder}"
 fi

 cd -
done
}

build_repo



