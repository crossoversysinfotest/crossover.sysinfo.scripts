#!/bin/bash

REPOSITORIES="
https://bitbucket.org/crossoversysinfotest/trd.lib.zmq.git
https://bitbucket.org/crossoversysinfotest/trd.lib.cppzmq.git
https://bitbucket.org/crossoversysinfotest/trd.lib.gtest.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.server.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.client.git
https://bitbucket.org/crossoversysinfotest/crossover.sysinfo.docs.git
"

# Return the default folder which git will checkout the repo
# Parameters:
#  1 - full repo name
# Return
#  Folder repo name
#
get_repo_folder() {
  local ret=$(echo "${1}" | awk -F '/' '{print $5}' | awk -F ".git" '{print $1}')
  echo $ret
} 

