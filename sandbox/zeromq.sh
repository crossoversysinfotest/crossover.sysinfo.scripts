#!/bin/bash
# Script to download, build and publish zeromq library

# Download zeromq
git clone https://github.com/zeromq/libzmq.git

cd libzmq

# Build zeromq
./autogen.sh
./configure
make

cd -

